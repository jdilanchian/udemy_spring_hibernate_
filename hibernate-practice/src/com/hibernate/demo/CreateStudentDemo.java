package com.hibernate.demo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernate.demo.entity.Student;

public class CreateStudentDemo {

	public static void main(String[] args) {

		// Create session factory
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Student.class)
				.buildSessionFactory();
		
		// Create session
		Session session = factory.getCurrentSession();
				
		try {
			// use the session object to save java object
			System.out.println("Creating new student object.");
			// create student object
			Student tempStudent1 = new Student("Paul", "Wall", "paul@gmail.com");
			Student tempStudent2 = new Student("John", "Doe", "paul@gmail.com");
			// start a transaction
			session.beginTransaction();
			// save the student object
			System.out.println("Saving the student.");
//			session.save(tempStudent1);
			session.save(tempStudent2);
			// commit transaction 
			session.getTransaction().commit();
			
			System.out.println("Last saved student's id is: " + tempStudent2.getId());
			
			// need new session for new action
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			Student myStudent = session.get(Student.class, tempStudent2.getId());
			
			System.out.println("Retrived student is " + myStudent);
			session.getTransaction().commit();
			
			// sample query from database
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			// the last name the name of the property in Java class and not the column name in DB
			List<Student> theStudents = 
					session.createQuery("from Student s where s.lastName = 'Doe'").getResultList(); 
			
			// just set the new value and the "COMMIT" will handle updating the DB table
			for(Student s : theStudents) {
				System.out.println(s);
				s.setLastName("Smith");
			}
			
			session.getTransaction().commit();
			
			// update using query
			session = factory.getCurrentSession();
			session.beginTransaction();
			session.createQuery("update Student s set s.lastName='Brick' where s.lastName = 'Wall'").executeUpdate();
			session.getTransaction().commit();
			
			// Delete object
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			int studentId = 1;
			Student delStudent= session.get(Student.class, studentId);
			if(delStudent != null) {
				session.delete(delStudent);
			}
			session.getTransaction().commit();		
			
			// Delete object using query
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			session.createQuery("delete from Student where id = '2'").executeUpdate();
			session.getTransaction().commit();
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			factory.close();
		}
	}
}
