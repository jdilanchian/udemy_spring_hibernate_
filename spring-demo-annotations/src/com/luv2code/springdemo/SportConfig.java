package com.luv2code.springdemo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

// java configuration => NO XML	
@Configuration
@PropertySource("classpath:sport.properties")
public class SportConfig {
	// define bean for our sad fortune service
	// sadFortuneService is the bean id
	@Bean 
	public FortuneService sadFortuneService() {
		return new SadFortuneService();
	}
	
	// define nbean for our swim coach and inject dependecy 
	
	@Bean 
	public Coach swimCoach() {
		return new SwimCoach(sadFortuneService());
	}
	
}
