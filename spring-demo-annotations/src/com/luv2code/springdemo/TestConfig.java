package com.luv2code.springdemo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestConfig {
	@Bean 
	public FortuneService fortuneService() {
		return new TestFortune();
	}
	
	@Bean 
	public TestCoach testCoach() {
		return new TestCoach(fortuneService());
	}

}
