package com.luv2code.springdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AnnotationsBeanScopeDemoApp {

	public static void main(String[] args) {
		
		// load spring config file
//		ClassPathXmlApplicationContext context = 
//				new ClassPathXmlApplicationContext("applicationContext.xml");
		AnnotationConfigApplicationContext context = new
				AnnotationConfigApplicationContext(TestConfig.class);
		// retrive bean from sping container
		
		Coach theCoach = context.getBean("testCoach", Coach.class);
		System.out.println(theCoach.getDailyFortune());
		
		context.close();
	}

}
