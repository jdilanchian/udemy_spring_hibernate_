package com.java.configbeans;

public interface Coach {

	public String getDailyWorkout();

	public String getDailyFortune();
	
}
