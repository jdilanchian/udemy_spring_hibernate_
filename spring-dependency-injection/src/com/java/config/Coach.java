package com.java.config;

public interface Coach {

	public String getDailyWorkout();

	public String getDailyFortune();
	
}
