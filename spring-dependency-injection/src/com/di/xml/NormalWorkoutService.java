package com.di.xml;

import org.springframework.stereotype.Component;

@Component
public class NormalWorkoutService implements WorkoutService {

	@Override
	public String getWorkout() {
		return "Normal workout string";
	}

}
