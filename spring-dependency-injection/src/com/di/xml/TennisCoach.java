package com.di.xml;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.EmitUtils;
import org.springframework.stereotype.Component;

public class TennisCoach implements Coach {

	private String emailAddress;
	private String team;
	
	public void setTeam(String theTeam) {
		team = theTeam;
	}
	
	public String getTeam() {
		return team;
	}

	private FortuneService fortuneService;
	private HopeService hopeService;
	
	// setter injection
	public void setHopeService(HopeService hopeService) {
		this.hopeService = hopeService;
	}

	private WorkoutService workoutService;
	
	// property injection
	public void setEmailAddress(String theEmailAddress) {
		System.out.println("in set email");
		emailAddress = theEmailAddress;
	}
	
	public String getEmailAddress() {
		return emailAddress;
	}
	
	public void setWorkoutService(WorkoutService theWorkoutService) {
		workoutService = theWorkoutService;
	}
	
	public String getDailyWorkout() {
		return workoutService.getWorkout();
	}

	// constructor injection
	public TennisCoach(FortuneService theFortuneService) {
		fortuneService = theFortuneService;
	}
	
	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}

	@Override
	public String getFixedWorkout() {
		return "Practice tennis";
	}

	@Override
	public String getDailyHope() {
		return hopeService.getHope();
	}
	
	public void initializationTasks() {
		System.out.println("initializations tasks go here");
	}
	
	public void distructionTasks() {
		System.out.println("destruction tasks go here");
		
	}
	
	
}
