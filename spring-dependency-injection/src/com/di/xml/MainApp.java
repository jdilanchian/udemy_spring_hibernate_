package com.di.xml;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainApp {

	public static void main(String[] args) {
		
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("applicationContext.xml");
		
		// Using constructor dependency injection alongside AutoWired 
		Coach theCoach = context.getBean("myCoach", Coach.class);
		TennisCoach tennisCoach = context.getBean("myCoach", TennisCoach.class);
		System.out.println(theCoach.getFixedWorkout()); 
		System.out.println(theCoach.getDailyFortune()); // XML dependency injection
		System.out.println(tennisCoach.getEmailAddress());
		System.out.println(tennisCoach.getTeam());
		//		System.out.println(tennisCoach.getDailyWorkout()); 
		System.out.println(theCoach.getDailyHope()); // set up setter injection
		
		context.close();

	}

}
