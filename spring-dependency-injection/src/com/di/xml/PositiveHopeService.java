package com.di.xml;

import org.springframework.stereotype.Component;

@Component
public class PositiveHopeService implements HopeService {

	@Override
	public String getHope() {
		return "This is the positive hope String";
	}

}
