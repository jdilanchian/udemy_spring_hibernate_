package com.di.xmlautowire;

public interface Coach {
	
	public String getDailyFortune();
	
	public String getDailyWorkout();
	
	public String getDailyHope();

	String getFixedWorkout();
	
}
