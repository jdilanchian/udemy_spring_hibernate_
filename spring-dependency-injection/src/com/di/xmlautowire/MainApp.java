package com.di.xmlautowire;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainApp {

	public static void main(String[] args) {
		
		ClassPathXmlApplicationContext contextAutoWire = 
				new ClassPathXmlApplicationContext("applicationContextAutoWire.xml");
		
		// Using constructor dependency injection alongside AutoWired 
		Coach theCoach = contextAutoWire.getBean("tennisCoach", Coach.class);
		TennisCoach tennisCoach = contextAutoWire.getBean("tennisCoach", TennisCoach.class);
		System.out.println(theCoach.getFixedWorkout()); // Simple output
		System.out.println(theCoach.getDailyFortune()); // Constructor injection XML + Autowire
		System.out.println(tennisCoach.getDailyWorkout()); // Setter injection XML + Autowire
		System.out.println(theCoach.getDailyHope()); // Field-injection XML + Autowire
		
		contextAutoWire.close();

	}

}
