package com.di.xmlautowire;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TennisCoach implements Coach {

	private FortuneService fortuneService;
	
	@Autowired
	private HopeService hopeService;
	
	private WorkoutService workoutService;
	
	@Autowired
	public void setWorkoutService(WorkoutService theWorkoutService) {
		workoutService = theWorkoutService;
	}
	
	public String getDailyWorkout() {
		return workoutService.getWorkout();
	}

	@Autowired
	public TennisCoach(FortuneService theFortuneService) {
		fortuneService = theFortuneService;
	}
	
	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}

	@Override
	public String getFixedWorkout() {
		return "Practice tennis";
	}

	@Override
	public String getDailyHope() {

		return hopeService.getHope();
	}
	
	@PostConstruct 
	public void init() {
		System.out.println("Post Construction tasks go here");
	}

	@PreDestroy
	public void destruct() {
		System.out.println("task before destruction");
	}
}
