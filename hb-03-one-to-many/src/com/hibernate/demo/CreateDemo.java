package com.hibernate.demo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernate.demo.entity.Course;
import com.hibernate.demo.entity.Instructor;
import com.hibernate.demo.entity.InstructorDetail;

public class CreateDemo {

	public static void main(String[] args) {

		// Create session factory
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.addAnnotatedClass(Course.class)
				.buildSessionFactory();
		
		// Create session
		Session session = factory.getCurrentSession();
		
		try {
			// from Instructor to InstructorDetail
			session.beginTransaction();
			Instructor tempInstructor = new Instructor("Joe", "Smith", "jsmith@yahoo.com");
			InstructorDetail tempInstructorDetail = new InstructorDetail("someurl.com", "chess");
			
			tempInstructor.setInstructorDetail(tempInstructorDetail);
			
			// will save the tempInstructorDetail as well because of the the Cascade.ALL
			session.save(tempInstructor);		
			
			session.getTransaction().commit();
			
			// Delete
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			int tempId = 1;
			Instructor tempInstructor2 = session.get(Instructor.class, tempId);
			if(tempInstructor2 != null) {
				session.delete(tempInstructor2);
			}
			session.getTransaction().commit();
					
			//Checking InstructorDetail to Instructor connection
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			int instructorDetailID = 2;
			InstructorDetail instructorDetail = session.get(InstructorDetail.class, instructorDetailID);
			if(instructorDetail != null ) {
				System.out.println(instructorDetail.getInstructor());
			}
			else {
				System.out.println("no instructor detail with ID " + instructorDetailID );
			}
			
			session.getTransaction().commit();
			
			// delete InstructorDetail without touching the Instructor
			/*
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			tempInstructorDetail = session.get(InstructorDetail.class, 2);
			// Throws exception without this
			tempInstructorDetail.getInstructor().setInstructorDetail(null);
			session.delete(tempInstructorDetail);
			session.getTransaction().commit();
			*/
			
			// Adding courses to instructor
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			int theId = 4;
			Instructor tempInstructor3 = new Instructor("John", "Doe", "jdoe@yahoo.com");
						
					
			Course anthro  = new Course("Anthro3");
			Course math = session.get(Course.class, 11);
			
			tempInstructor3.addCourse(math);
			tempInstructor3.addCourse(anthro);
			session.update(math);
			session.save(anthro);
			session.save(tempInstructor3);
				
			session.getTransaction().commit();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			session.close();
			factory.close();
		}
	}
}
