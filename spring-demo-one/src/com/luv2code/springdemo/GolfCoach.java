package com.luv2code.springdemo;

public class GolfCoach implements Coach {

	private FortuneService golfFurtuneSerivce;	

	public FortuneService getGolfFurtuneSerivce() {
		return golfFurtuneSerivce;
	}

	public void setGolfFurtuneSerivce(FortuneService golfFurtuneSerivce) {
		this.golfFurtuneSerivce = golfFurtuneSerivce;
	}

	@Override
	public String getDailyWorkout() {
		// TODO Auto-generated method stub
		return "golf daily workout";
	}

	@Override
	public String getDailyFortune() {
		// TODO Auto-generated method stub
		return golfFurtuneSerivce.getFortune();
				
	}

}
