package com.luv2code.springdemo;

import java.util.Random;

public class FortuneService2nd implements FortuneService {

	private String[] data = {
		"1st fortune",
		"2nd fortune",
		"3rd fortune"
	};
	
	private Random random = new Random();
	
	@Override
	public String getFortune() {
		int index = random.nextInt(data.length);
		// TODO Auto-generated method stub
		return data[index];
	}

}
