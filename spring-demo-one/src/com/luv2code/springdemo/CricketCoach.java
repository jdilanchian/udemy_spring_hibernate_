package com.luv2code.springdemo;

public class CricketCoach implements Coach {

	private FortuneService fortuneService; 
	
	// new fields for string injection
	private String emailAddress;
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		System.out.println("cricket coach inside setter method - setemailAddress");
		this.emailAddress = emailAddress;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		System.out.println("cricket coach inside setter method - setTeam");
		this.team = team;
	}

	private String team; 
	
	public CricketCoach() {
		System.out.println("CricketCoack: inside no-arg constructor");
	}
		
	public void setFortuneService(FortuneService fortuneService) {
		System.out.println("Cricket coach inside the Setter method");
		this.fortuneService = fortuneService;
	}

	@Override
	public String getDailyWorkout() {
		// TODO Auto-generated method stub
		return "daily workout from Cricket Coach";
	}

	@Override
	public String getDailyFortune() {
		// TODO Auto-generated method stub
		return fortuneService.getFortune();
	}

}
