package com.luv2code.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HelloSpringApp {

	public static void main(String[] args) {
		
		// load the spring configuration file
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("applicationContext.xml");
		// retrive bean from spring container
		// Coach.class adds type safety
		Coach theCoach = context.getBean("myCoach", Coach.class);
		// call metos on the bean
		System.out.println(theCoach.getDailyWorkout());
		// new Constructor Injected method
		System.out.println(theCoach.getDailyFortune());
		
		// close the context
		context.close();

	}

}
